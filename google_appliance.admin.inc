<?php
/**
 * @file
 *    admin callbacks for the Google Appliance module
 */
define('SGA_KEYMATCH_MAX', 100000); // Maximum number of keymatchs we may display (arbitrary).
define('SGA_KEYMATCH_LIMIT', 50); // Number of keymatchs per page.

/**
 * Implements hook_admin_settings().
 *    displays the Search Google Appliance module settings page.
 *
 * @ingroup forms
 * @see google_appliance.helpers.inc
 * @see google_appliance_admin_settings_validate()
 * @see google_appliance_admin_settings_submit()
 */
function google_appliance_admin_settings($form) {

  // grab current settings
  $settings = _google_appliance_get_settings();

  // connection information
  $form['connection_info'] = array(
    '#title' => t('Connection Information'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['connection_info']['hostname'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Search Appliance Host Name'),
    '#description' => t('Valid URL or IP address of the GSA device, including <em>http://</em> or <em>https://</em>. Do <b>not</b> include <em>/search</em> at the end, or a trailing slash, but you should include a port number if needed. Example: <em>http://my.gsabox.com:8443</em>'),
    '#default_value' => $settings['hostname'],
    '#required' => TRUE,
  );
  $form['connection_info']['collection'] = array(
    '#type' => 'textfield',
    '#title' => t('Collection'),
    '#description' => t('The name of a valid collection on the GSA device (case sensitive).'),
    '#default_value' => $settings['collection'],
    '#required' => TRUE,
  );
  $form['connection_info']['frontend'] = array(
    '#type' => 'textfield',
    '#title' => t('Frontend client'),
    '#description' => t('The name of a valid frontend client on the GSA device (case sensitive).'),
    '#default_value' => $settings['frontend'],
    '#required' => TRUE,
  );
  $form['connection_info']['timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Search Timeout'),
    '#description' => t('Length of time to wait for response from the GSA device before giving up (timeout in seconds).'),
    '#default_value' => $settings['timeout'],
    '#required' => TRUE,
  );

  // Authentication for administrative API.
  $form['authentication'] = array(
    '#title' => t('Authentication (Administrative API)'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('You may provide credentials to administer the GSA from Drupal. Additional tabs will show up depending on your permissions.'),
  );
  $form['authentication']['username'] = array(
    '#type' => 'textfield',
    '#title' => t('User Name'),
    '#description' => t('A user name that has an Admin Console administrator account on the GSA.'),
    '#default_value' => $settings['username'],
  );
  $form['authentication']['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => t('The password for the Admin Console account.'),
    '#default_value' => '', // Don't reveal the password in the form.
  );
  $form['authentication']['verify_ssl'] = array(
    '#type' => 'checkbox',
    '#title' => t('Verify SSL certificate on the GSA'),
    '#description' => t('Uncheck this box if your GSA uses a self-signed certificate.'),
    '#default_value' => $settings['verify_ssl'],
  );

  // search query parameter configuration
  $form['query_param'] = array(
    "#title" => t("Search Query Parameter Setup"),
    "#type" => "fieldset",
    "#collapsible" => TRUE,
    "#collapsed" => FALSE,
  );
  $form['query_param']['autofilter'] = array(
    '#type' => 'select',
    '#title' => t('Search Results Auto-Filtering Options'),
    '#default_value' => $settings['autofilter'],
    '#options' => array(
      '0' => t('No filtering'),
      's' => t('Duplicate Directory Filter'),
      'p' => t('Duplicate Snippet Filter'),
      '1' => t('Both Duplicate Directory and Duplicate Snippet Filters')
    ),
    '#description' => t('Learn more about GSA auto-filtering <a href="@gsa-doc-af">here</a>. In general, employing both filters enhances results, but sites with smaller indexes may suffer from over-filtered results.', array('@gsa-doc-af' => 'http://code.google.com/apis/searchappliance/documentation/68/xml_reference.html#request_filter_auto') ),
  );
  if (module_exists('locale')) {
    $form['query_param']['google_appliance_language_filter_toggle'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Language Filtering'),
      '#default_value' => $settings['language_filter_toggle'],
    );
    $form['query_param']['google_appliance_language_filter_options'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Restrict searches to specified languages'),
      '#default_value' => $settings['language_filter_options'],
      '#options' => array(
        '***CURRENT_LANGUAGE***' => t("Current user's language"),
        '***DEFAULT_LANGUAGE***' => t("Default site language"),
      ) + locale_language_list(),
      '#states' => array(
        'visible' => array(
          ':input[name=google_appliance_language_filter_toggle]' => array('checked' => TRUE),
        ),
      ),
      '#description' => t('If there are no results in the specified language, the search appliance is expected to return results in all languages.'),
    );
  }
  $form['query_param']['query_inspection'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Query Inspection'),
    '#description' => t('Inspect the search query parameters sent to the GSA device in the Drupal message area every time a search is performed. Only really useful for sites not using the Devel module, as dsm() provides more information. The inspector is only shown to administrators, but should be disabled in a production environment.'),
    '#default_value' => $settings['query_inspection'],
  );

  // search interface settings
  $form['display_settings'] = array(
    '#title' => t('Search Interface Settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['display_settings']['search_title'] = array(
    '#title' => t('Search Name'),
    '#type' => 'textfield',
    '#default_value' => $settings['search_title'],
    '#description' => t('Serves as the page title on results pages and the default menu item title'),
    '#required' => TRUE,
  );
  $form['display_settings']['results_per_page'] = array(
    '#title' => t('Results per page'),
    '#type' => 'textfield',
    '#default_value' => $settings['results_per_page'],
    '#description' => t('Number of results to show on the results page. More results will be available via a Drupal pager.'),
    '#required' => TRUE,
  );
  $form['display_settings']['spelling_suggestions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Spelling Suggestions'),
    '#default_value' => $settings['spelling_suggestions'],
  );
  $form['display_settings']['advanced_search_reporting'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Advanced Search Reporting'),
    '#default_value' => $settings['advanced_search_reporting'],
    '#description' => t('Learn more about !this_feature. You need to enable Advanced Search Reporting on the front end client. The device should provide a file named "/clicklog_compiled.js" when using the search interface on the GSA.', array('!this_feature' => l(t('this feature'), 'http://www.google.com/support/enterprise/static/gsa/docs/admin/70/gsa_doc_set/xml_reference/advanced_search_reporting.html'))),
  );
  $form['display_settings']['google_appliance_onebox_modules'] = array(
    '#type' => 'textarea',
    '#title' => t('Onebox modules'),
    '#description' => t('A list of Onebox modules, one per line. Each module listed will have a corresponding block. These blocks must be placed via the block configuration page, or another layout mechanism like Context or Panels.'),
    '#default_value' => implode("\n", $settings['onebox_modules']),
  );
  $form['#submit'][] = 'google_appliance_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Implements hook_admin_settings_validate().
 *    form validation beyond FAPI '#required'
 */
function google_appliance_admin_settings_validate($form, &$form_state) {

  // host name should be a valid-format URL or IPv4 address including 'http(s)://'
  if ( filter_var($form_state['values']['hostname'], FILTER_VALIDATE_URL) === FALSE) {
    form_set_error(
      'hostname',
      t('GSA Host name must be a valid-format URL or IPv4 address, including <em>http(s)://</em>. Example: http://my.googlebox.net.')
    );
  }

  // timeout should be a reasonable number seconds
  $timeout_validate_options = array( 'options' => array(
    'max_range' => 30,
    'min_range' => 3,
  ));
  if ( filter_var($form_state['values']['timeout'], FILTER_VALIDATE_INT, $timeout_validate_options) === FALSE ) {
    form_set_error(
      'timeout',
      t('Search Timeout should be an integer from @min - @max, indicating the number of seconds to wait before the search request times out.', array(
        '@min' => $timeout_validate_options['options']['min_range'],
        '@max' => $timeout_validate_options['options']['max_range'],
      ))
    );
  }

  // results per page must be on a range that will be accepted by the device upon querying
  $results_per_page_validate_options = array('options' => array(
    'max_range' => 1000,
    'min_range' => 1
  ));
  if ( filter_var($form_state['values']['results_per_page'], FILTER_VALIDATE_INT, $results_per_page_validate_options) === FALSE ) {
    form_set_error(
      'results_per_page',
      t('Results per page should be an integer from @min - @max.', array(
        '@min' => $results_per_page_validate_options['options']['min_range'],
        '@max' => $results_per_page_validate_options['options']['max_range'],
      ))
    );
  }
}

/**
 * Implements hook_admin_settings_submit().
 *    submit handler for admin settings
 */
function google_appliance_admin_settings_submit($form, &$form_state) {

  $field_keys =  array(
    'hostname',
    'collection',
    'frontend',
    'timeout',
    'username',
    'verify_ssl',
    'autofilter',
    'query_inspection',
    'search_title',
    'spelling_suggestions',
    'results_per_page',
    'advanced_search_reporting',
  );

  // save settings
  foreach ($field_keys as $field) {
    variable_set('google_appliance_' . $field, trim($form_state['values'][$field]));
  }

  // The default value of 'password' is empty.
  // Update password only if user has set a value.
  if (!empty($form_state['values']['password'])) {
    variable_set('google_appliance_password', trim($form_state['values']['password']));
  }

  // refresh settings getter
  $settings = _google_appliance_get_settings(TRUE);

  // make the 'search_title' setting change take effect right away
  menu_rebuild();
}

/**
 * Form callback - list keymatchs.
 *
 * @see google_appliance_menu().
 */
function google_appliance_admin_keymatch_view() {
  $startLine = isset($_GET['page']) ? $_GET['page'] * SGA_KEYMATCH_LIMIT : 0;
  $query = empty($_SESSION['google_appliance']['keymatch_query']) ? NULL : $_SESSION['google_appliance']['keymatch_query'];
  $count = _google_appliance_admin_keymatchs_count($query);
  pager_default_initialize($count, SGA_KEYMATCH_LIMIT, 0);

  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search for keymatches containing'),
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['filter']['query'] = array(
    '#type' => 'textfield',
    '#default_value' => $query,
  );
  $form['filter']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );

  $form['pager_top'] = array('#markup' => theme('pager'));

  $headers = array(
    'terms' => array('data' => t('Search Terms')),
    'occurrence' => array('data' => t('Terms Occur As')),
    'url' => array('data' => t('URL for Match')),
    'title' => array('data' => t('Title for Match')),
  );
  $options = _google_appliance_admin_keymatchs_retrieve($startLine, SGA_KEYMATCH_LIMIT, $query);

  // Sanitize data.
  foreach ($options as $key => $option) {
    $options[$key] = array_map('check_plain', $option);
  }

  $table = array(
    'header' => $headers,
    'rows' => $options,
    'empty' => t('There is no keymatch.'),
  );

  $form['keymatch'] = array('#markup' => theme('table', $table));

  $form['pager_bottom'] = array('#markup' => theme('pager'));

  return $form;
}

/**
 * Form submit handler - view keymatchs.
 *
 * Store search string in session.
 *
 * @see google_appliance_admin_keymatch_view().
 */
function google_appliance_admin_keymatch_view_submit($form, &$form_state) {
  $_SESSION['google_appliance']['keymatch_query'] = empty($form_state['values']['query']) ? NULL : $form_state['values']['query'];
}

/**
 * Form callback - edit keymatchs.
 *
 * @see google_appliance_menu().
 */
function google_appliance_admin_keymatch_edit() {
  $startLine = isset($_GET['page']) ? $_GET['page'] * SGA_KEYMATCH_LIMIT : 0;
  $count = _google_appliance_admin_keymatchs_count();
  pager_default_initialize($count, SGA_KEYMATCH_LIMIT, 0);

  $form['pager_top'] = array('#markup' => theme('pager'));

  $form['keymatch'] = array(
    '#tree' => TRUE,
    '#theme' => 'google_appliance_table_keymatchs',
    '#header' => array(
      'delete' => t('Delete'),
      'terms' => t('Search terms'),
      'occurrence' => t('Terms occur as'),
      'url' => t('URL for match'),
      'title' => t('Title for match'),
    ),
    '#empty' => t('There is no keymatch.'),
  );

  $options = _google_appliance_admin_keymatchs_retrieve($startLine, SGA_KEYMATCH_LIMIT, $query);
  foreach ($options as $key => $option) {
    $form['keymatch'][$key] = array(
      'delete' => array(
        '#type' => 'checkbox',
        '#default_value' => FALSE,
      ),
      'terms' => array(
        '#type' => 'textfield',
        '#default_value' => $option['terms'],
        '#size' => 20,
      ),
      'occurrence' => array(
        '#type' => 'select',
        '#default_value' => $option['occurrence'],
        '#options' => drupal_map_assoc(array('KeywordMatch', 'PhraseMatch', 'ExactMatch')),
      ),
      'url' => array(
        '#type' => 'textfield',
        '#default_value' => $option['url'],
        '#size' => 30,
      ),
      'title' => array(
        '#type' => 'textfield',
        '#default_value' => $option['title'],
        '#size' => 40,
      ),
    );
  }

  $form['start_line'] = array(
    '#type' => 'hidden',
    '#value' => $startLine,
  );

  $form['pager_bottom'] = array('#markup' => theme('pager'));

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Form validation handler - edit keymatchs.
 *
 *
 * @see google_appliance_admin_keymatch_edit().
 */
function google_appliance_admin_keymatch_edit_validate($form, $form_state) {
  foreach ($form_state['values']['keymatch'] as $name => $keymatch) {
    // If keymatch set to be deleted then skip validation.
    if ($keymatch['delete']) {
      continue;
    }
    // Otherwise, validate data.
    google_appliance_admin_keymatch_validate($name, $keymatch);

  }
}

/**
 * Form submit handler - edit keymatchs.
 *
 * @see google_appliance_admin_keymatch_edit().
 */
function google_appliance_admin_keymatch_edit_submit($form, &$form_state) {
  $startLine = $form_state['values']['start_line'];
  $oldKeymatchs = _google_appliance_admin_keymatchs_retrieve($startLine, SGA_KEYMATCH_LIMIT);
  $newKeymatchs = $form_state['values']['keymatch'];

  foreach ($newKeymatchs as $key => $newKeymatch) {
    if ($newKeymatch['delete']) {
      $newKeymatchs[$key] = array('terms' => '', 'occurrence' => '', 'url' => '', 'title' => '');
    }
  }

  _google_appliance_admin_keymatchs_update($startLine, $oldKeymatchs, $newKeymatchs);
}

/**
 * Form callback - add keymatch.
 *
 * @see google_appliance_menu().
 */
function google_appliance_admin_keymatch_add() {
  $form['keymatch'] = array(
    '#tree' => TRUE,
    '#theme' => 'google_appliance_table_keymatchs',
    '#header' => array(
      'terms' => t('Search terms'),
      'occurrence' => t('Terms occur as'),
      'url' => t('URL for match'),
      'title' => t('Title for match'),
    ),
  );
  for ($i = 0; $i < 10; $i++) {
    $form['keymatch'][] = array(
      'terms' => array(
        '#type' => 'textfield',
        '#size' => 20,
      ),
      'occurrence' => array(
        '#type' => 'select',
        '#options' => drupal_map_assoc(array('KeywordMatch', 'PhraseMatch', 'ExactMatch')),
      ),
      'url' => array(
        '#type' => 'textfield',
        '#size' => 30,
      ),
      'title' => array(
        '#type' => 'textfield',
        '#size' => 40,
      ),
    );
  }

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Form validation handler - add keymatchs.
 *
 * @see google_appliance_admin_keymatch_add().
 */
function google_appliance_admin_keymatch_add_validate($form, &$form_state) {
  foreach ($form_state['values']['keymatch'] as $key => $keymatch) {
    if (empty($keymatch['terms']) && empty($keymatch['url']) && empty($keymatch['title'])) {
      // User left all fields empty: we won't insert it in the GSA.
      unset($form_state['values']['keymatch'][$key]);
    }
    else {
      // User filled in at least 1 field: validate data.
      google_appliance_admin_keymatch_validate($key, $keymatch);
    }
  }
}

/**
 * Form submit handler - add keymatch to the GSA.
 *
 * @see google_appliance_admin_keymatch_add().
 */
function google_appliance_admin_keymatch_add_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/config/search/google_appliance/keymatch';
  _google_appliance_admin_keymatchs_append($form_state['values']['keymatch']);
}

/**
 * Validates a single keymatch.
 *
 * The GSA would not return the reason why a keymatch does not validate.
 * We replicate the validation rules of the GSA here.
 *
 * @param $element
 *   Form element.
 * @param $keymatch
 *   Array with keys 'terms', 'occurrence', 'url' and 'title'.
 *
 * @see google_appliance_admin_keymatch_add().
 * @see google_appliance_admin_keymatch_edit().
 */
function google_appliance_admin_keymatch_validate($element, $keymatch) {
  // 'Search terms' is mandatory.
  if (empty($keymatch['terms'])) {
    form_set_error('keymatch][' . $element . '][terms', t('%field cannot be left empty.', array('%field' => 'Search Terms')));
  }

  // 'URL for match' is mandatory.
  if (empty($keymatch['url'])) {
    form_set_error('keymatch][' . $element . '][url', t('%field cannot be left empty.', array('%field' => 'URL for Match')));
  }

  // 'URL for match' must be a valid URL.
  if (!valid_url($keymatch['url'], TRUE)) {
    form_set_error('keymatch][' . $element . '][url', t('Invalid URL %url.', array('%url' => $keymatch['url'])));
  }

  // 'URL for match' must contain a path.
  // e.g. 'http://example.net/abcd' validates, 'http://example.net' does not.
  $path = parse_url($keymatch['url'], PHP_URL_PATH);
  if (empty($path)) {
    form_set_error('keymatch][' . $element . '][url', t('URL %url contains no path.', array('%url' => $keymatch['url'])));
  }
}

/**
 * Get a valid authentication token - either a new one from the GSA or an old one stored in the session.
 *
 * @param $force_new
 *   If TRUE, then a fresh token will be retrieved from the GSA.
 *
 * @return
 *   Authentication token.
 *
 * @throws Exception
 *   If a token could not be retrieved.
 *
 * @see http://www.google.com/support/enterprise/static/gsa/docs/admin/70/gsa_doc_set/acapi_protocol/acapi_protocol.html#1078166
 */
function _google_appliance_admin_token($force_new = FALSE) {
  if (!isset($_SESSION['google_appliance']['token']) || $force_new) {

    // Build URL for authentication.
    // Speaking to port 8443 of the GSA requires to use the https scheme.
    $settings = _google_appliance_get_settings();
    $host = parse_url($settings['hostname'], PHP_URL_HOST);
    $url = 'https://' . $host . ':8443/accounts/ClientLogin';

    $params = array(
      'Email' => rawurlencode($settings['username']),
      'Passwd' => rawurlencode($settings['password']),
    );
    $options = array(
      CURLOPT_HTTPHEADER => array('Content-Type: application/x-www-form-urlencoded'),
    );
    if (!$settings['verify_ssl']) {
      $options += array(
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
      );
    }
    $response = _curl_post($url, $params, $options);

    if ($response['is_error']) {
      watchdog('google_appliance', $response['response'], array(), WATCHDOG_ERROR);
      throw new Exception(t($response['response']));
    }

    // Extract token from response.
    $token = '';
    $lines = preg_split("/\r\n|\n|\r/", $response['response']);
    foreach ($lines as $line) {
      list($name, $token) = explode('=', $line, 2);
      if ($name == 'Auth') {
        break;
      }
    }

    if (empty($token)) {
      watchdog('google_appliance', 'Could not get authentication token. GSA replied: @response.', array('@response' => $response['response']), WATCHDOG_ERROR);
      throw new Exception(t('Could not authenticate to Google Search Appliance. Did you set a valid username/password in the <a href="@url">settings</a> ?', array('@url' => url('admin/config/search/google_appliance'))));
    }

    $_SESSION['google_appliance']['token'] = $token;
  }

  return $_SESSION['google_appliance']['token'];
}

/**
 * Send a request to the keymatchs endpoint on the GSA.
 *
 * @param $method
 *   HTTP method: either GET (default) or PUT.
 * @param $data
 *   Data to send.
 * @param $force_new_token
 *   If TRUE, then get a fresh authentication token from the GSA.
 *
 * @return
 *   XML returned by the GSA.
 *
 * @throws Exception
 *   If the request failed.
 */
function _google_appliance_admin_keymatchs($method, $data = NULL, $force_new_token = FALSE) {
  $token = _google_appliance_admin_token($force_new_token);

  // Build keymatchs endpoint from the settings.
  $settings = _google_appliance_get_settings();
  $parts = parse_url($settings['hostname']);
  $url = $parts['scheme'] . '://' . $parts['host'] . ':8000/feeds/keymatch/' . $settings['frontend'];

  // Build headers specific to the Admin API.
  $options = array(
    CURLOPT_HTTPHEADER => array(
      'Content-Type: application/atom+xml',
      'Authorization: GoogleLogin auth=' . rawurlencode($token),
    ),
  );

  // Trigger request to GSA.
  switch ($method) {
    case 'PUT':
      $result = _curl_put($url, $data, $options);
      break;
    case 'GET':
    default:
      $result = _curl_get($url, $data, $options);
  }

  // Check if Curl returned an error.
  if ($result['is_error']) {
    watchdog('google_appliance', $result['response'], array(), WATCHDOG_ERROR);
    throw new Exception(t($result['response']));
  }

  // Check if GSA returned an internal error.
  if ($result['response'] == 'Internal Error') {
    static $new_token = FALSE;
    if (!$new_token) {
      // Token may have expired: get a new one.
      $new_token = TRUE;
      return _google_appliance_admin_keymatchs($method, $data, TRUE);
    }
    else {
      // Error persists with a fresh token: throw exception.
      watchdog('google_appliance', 'GSA returned internal error. URL: @url. Data: @data.', array('@url' => $url, '@data' => print_r($data, TRUE)), WATCHDOG_ERROR);
      throw new Exception(t('Google Search Appliance returned: Internal Error.'));
    }
  }

  // Check if GSA returned valid XML.
  $xml = simplexml_load_string($result['response']);
  if (!$xml) {
    watchdog('google_appliance', 'Could not parse reponse from GSA. URL @url. Data: @data. Response: @response.', array('@url' => $url, '@data' => print_r($data, TRUE), '@response' => $result['response']), WATCHDOG_ERROR);
    throw new Exception(t('Could not parse response from Google Search Appliance.'));
  }

  // Check if XML returned by the GSA contains an error message.
  // e.g. someone modified keymatchs on the GSA while the user was editing.
  $xml->registerXPathNamespace('g', 'http://schemas.google.com/g/2005');
  $errors = $xml->xpath('//g:error/g:internalReason');
  if (!empty($errors)) {
    // The GSA might return the same error message in multiple XML elements.
    // Make sure an error message shows up only once.
    $errors = array_unique($errors);
    throw new Exception(t('Google Search Appliance returned: @errors', array('@errors' => implode(", ", $errors))));
  }

  return $xml;
}

/**
 * Get list of keymatchs.
 *
 * @param $startLine
 *   Start line of the configuration table.
 * @param $maxLines
 *   Maximum number of lines to return.
 * @param $query
 *   Query string to perform a full-text search. Defaults to NULL (no search).
 *
 * @return
 *   Array of keymatchs, each keymatch being an array with keys 'terms', 'occurrence', 'url' and 'title'.
 */
function _google_appliance_admin_keymatchs_retrieve($startLine, $maxLines, $query = NULL) {
  // Request XML from GSA.
  $data = array(
    'startLine' => $startLine,
    'maxLines' => $maxLines,
    'query' => $query,
  );
  try {
    $xml = _google_appliance_admin_keymatchs('GET', $data);
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
    return array();
  }

  // Parse XML response.
  // Keymatchs are <gsa:content name="foobar"> elements where foobar is a number.
  $keymatchs = array();
  $keys = array('terms', 'occurrence', 'url', 'title');
  foreach ($xml->xpath('//gsa:content') as $content) {
    $name = (string) $content['name'];
    if (is_numeric($name)) {
      $values = str_getcsv((string) $content);
      $keymatchs[$name] = array_combine($keys, $values);
    }
  }

  // The GSA admin interface sorts keymatchs by name.
  ksort($keymatchs);

  return $keymatchs;
}

/**
 * Count the total number of keymatchs stored on the GSA.
 *
 * The GSA API does not expose this value directly.
 * As a workaround, we request all keymatchs and check how many records are returned.
 *
 * @param $query
 *   Query string to perform a full-text search. Defaults to NULL (no search).
 *
 * @return
 *   The number of keymatchs stored on the GSA, or FALSE if an error occurred.
 */
function _google_appliance_admin_keymatchs_count($query = NULL) {
  $data = array(
    'startLine' => 0,
    'maxLines' => SGA_KEYMATCH_MAX,
    'query' => $query,
  );

  try {
    $xml = _google_appliance_admin_keymatchs('GET', $data);
  }
  catch (Exception $e) {
    return FALSE;
  }

  $numLines =  (string) array_pop($xml->xpath("//gsa:content[@name='numLines']"));
  return $numLines;
}

/**
 * Append new keymatchs in the GSA config.
 *
 * @param $keymatchs
 *   Array of keymatchs, each keymatch being an array with keys 'terms', 'occurrence', 'url' and 'title'.
 *
 * @see http://www.google.com/support/enterprise/static/gsa/docs/admin/70/gsa_doc_set/acapi_protocol/acapi_protocol.html#1089555
 */
function _google_appliance_admin_keymatchs_append($keymatchs) {
  $lines = _google_appliance_keymatchs_to_csv($keymatchs);

  $xml = new SimpleXMLElement('<entry></entry>');
  $xml->addAttribute('xmlns', 'http://www.w3.org/2005/Atom');
  $xml->addAttribute('xmlns:xmlns:gsa', 'http://schemas.google.com/gsa/2007');
  $xml->addChild('xmlns:gsa:content', 'append')->addAttribute('name', 'updateMethod');
  $xml->addChild('xmlns:gsa:content', $lines)->addAttribute('name', 'newLines');

  try {
    _google_appliance_admin_keymatchs('PUT', $xml->asXML());
    drupal_set_message(t('Successfully added @keymatches.', array('@keymatches' => format_plural(count($keymatchs), '1 keymatch', '@count keymatches'))));
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
  }
}

/**
 * Update/delete keymatchs in the GSA config.
 *
 * @param $startLine
 *   Start line of the configuration table.
 * @param $oldKeymatchs
 *   Array of original keymatchs, each keymatch being an array with keys 'terms', 'occurrence', 'url' and 'title'.
 * @param $newKeymatchs
 *   Array of new keymatchs.
 *
 * @see http://www.google.com/support/enterprise/static/gsa/docs/admin/70/gsa_doc_set/acapi_protocol/acapi_protocol.html#1089555
 */
function _google_appliance_admin_keymatchs_update($startLine, $oldKeymatchs, $newKeymatchs) {
  $oldLines = _google_appliance_keymatchs_to_csv($oldKeymatchs);
  $newLines = _google_appliance_keymatchs_to_csv($newKeymatchs);

  $xml = new SimpleXMLElement('<entry></entry>');
  $xml->addAttribute('xmlns', 'http://www.w3.org/2005/Atom');
  $xml->addAttribute('xmlns:xmlns:gsa', 'http://schemas.google.com/gsa/2007');
  $xml->addChild('xmlns:gsa:content', 'update')->addAttribute('name', 'updateMethod');
  $xml->addChild('xmlns:gsa:content', $startLine)->addAttribute('name', 'startLine');
  $xml->addChild('xmlns:gsa:content', $oldLines)->addAttribute('name', 'originalLines');
  $xml->addChild('xmlns:gsa:content', $newLines)->addAttribute('name', 'newLines');

  try {
    _google_appliance_admin_keymatchs('PUT', $xml->asXML());
    drupal_set_message(t('Successfully updated @keymatches.', array('@keymatches' => format_plural(count($oldKeymatchs), '1 keymatch', '@count keymatches'))));
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
  }

}

/**
 * Convert an array of keymatchs into a CSV string.
 *
 * @param $keymatchs
 *   Array of keymatchs, each keymatch being an array with keys 'terms', 'occurrence', 'url' and 'title'.
 * @return
 *   CSV string.
 */
function _google_appliance_keymatchs_to_csv($keymatchs = array()) {
  $keys = drupal_map_assoc(array('terms', 'occurrence', 'url', 'title'));
  $lines = array();

  foreach ($keymatchs as $keymatch) {
    $keymatch = array_intersect_key($keymatch, $keys);
    // Wrap values with quotes so users can insert commas and we follow the csv spec.
    // @see http://www.csvreader.com/csv_format.php.
    foreach ($keymatch as $key => $value) {
      $keymatch[$key] = "\"$value\"";
    }
    $lines[] = implode($keymatch, ',');
  }

  return implode($lines, "\n");
}
